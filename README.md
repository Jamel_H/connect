using NUnit.Framework;
using OfficeOpenXml;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium.Android;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace SCRIPT
{
    public class Scripts
    {
        public IWebDriver _driverWEB;
        IWebElement element;
        public int SCENARIOPos, attemps = 0, CheckTheRightProfileImg = 0;
        public AndroidDriver<AndroidElement> _driverANDROID;
        public WebDriverWait _wait;
        public List<Produits> _pdt = new List<Produits>();
        public List<Gar> _gar = new List<Gar>();
        ExcelPackage package = new ExcelPackage(new FileInfo(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\for Vone.xlsx"));
        ExcelPackage docGar = new ExcelPackage(new FileInfo(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\GAR test.xlsx"));

        public void Organizational_Map()
        {

            _gar = ListEmployees();
            


            //FindById("username").SendKeys("TestingADM");
            _driverWEB.Navigate().GoToUrl("https://disneyeu.kronos.net/wfc/logon?splogonexception=true");
            
            //Access WebSite Kronos                                                                             
            //_wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"kronos\"]/ div[5]/p[3]/a"))).Click();
            //Thread.Sleep(2000);    

            //Connexion Testing STUDIO
            //FindById("username").Clear();
            FindById("username").SendKeys("xbrice");
            //FindById("passInput").Clear();
            FindById("passInput").SendKeys("1058971");
            FindById("loginSubmit").Click();
            
            //Scenario de test
            Thread.Sleep(3000);
            //Autoriser le Flash
            new Actions(_driverWEB).SendKeys(OpenQA.Selenium.Keys.Tab).Perform();
            new Actions(_driverWEB).SendKeys(OpenQA.Selenium.Keys.Tab).Perform();
            new Actions(_driverWEB).SendKeys(OpenQA.Selenium.Keys.Enter).Perform();
            
            //Chargement de Widgets
            Thread.Sleep(7000);
            //Chargement de premier iframe
            _driverWEB.SwitchTo().Frame(1);
            Thread.Sleep(2000);
            //Chargement de deuxieme iframe
            _driverWEB.SwitchTo().Frame(0);
            FindById("setupSearch_value").SendKeys("Jobs and Organizational Map");
            
            new Actions(_driverWEB).SendKeys(OpenQA.Selenium.Keys.Down).Perform();
            new Actions(_driverWEB).SendKeys(OpenQA.Selenium.Keys.Down).Perform();
            new Actions(_driverWEB).SendKeys(OpenQA.Selenium.Keys.Enter).Perform();

            //Scenario Jobs and Organizational Map
            Thread.Sleep(2000);
            _driverWEB.SwitchTo().Frame(0);
            //Organizational Maps
            Thread.Sleep(2000);
            FindByXPath("//*[@id=\"mainWidgetContent\"]/ui-view/ng-form/div/div/div/krn-navbar/ul/li[3]/a").Click();
            
            //First Node
            Thread.Sleep(1000);
            String Prefix = "//*[@id=\"mainWidgetContent\"]/ui-view/ng-form/div/div/div/div/div/div/fieldset/krn-tree-configurator/div/div/div/div/krn-lazy-tree/ul/li/ul/li/";
            String Postfix = "span/span[1]";
            FindByXPath(Prefix+Postfix).Click();

            String Word = Prefix + "span/span[3]";
            String Path = FindByXPath(Word).Text;
            string w1 = "DRP";
            
            //test nbr de node
            var Nodetext = FindByXPath("//*[@id=\"mainWidgetContent\"]/ui-view/ng-form/div/div/div/div/div/div/fieldset/krn-tree-configurator/div/div/div/div/krn-lazy-tree/ul/li/ul/li/ul");
            
            int Node1 = nodeNumber(Nodetext);
            
            for(int i = 1; i <= Node1; i++)
            {
                String word2 = Prefix + "ul/li[" + i + "]/span/span[3]";
                String Path2 = FindByXPath(word2).Text;
                String w2 = "/" + getWord(Path2);
                w1 = w1 + w2;

                Thread.Sleep(1000);
                FindByXPath(Prefix + "ul/li[" + i + "]/" + Postfix).Click();
                var tt = FindByXPath(Prefix + "ul/li[" + i + "]/" + Postfix).Text;
                var x = Prefix+"ul/li[" + i + "]/ul";
                var y = FindByXPath(x);
                int z = nodeNumber(y);

                int p = 0;
                for (int j = 1; j <= z; j++)
                {
                    String word3 = Prefix + "ul/li[" + i + "]/ul/li[" + j + "]/span/span[3]";
                    String Path3 = FindByXPath(word3).Text;
                    String w3 = "/" + getWord(Path3);
                    w1 = w1 + w3;

                    Thread.Sleep(1000);
                    FindByXPath(Prefix + "ul/li[" + i + "]/ul/li[" + j + "]/" + Postfix).Click();
                    var x1 = Prefix + "ul/li[" + i + "]/ul/li[" + j + "]/ul";
                    var y1 = FindByXPath(x1);
                    int z1 = nodeNumber(y1);
                    for(int k = 1; k <= z1; k++)
                    {
                        String word4 = Prefix + "ul/li[" + i + "]/ul/li[" + j + "]/ul/li[" + k + "]/span/span[3]";
                        String Path4 = FindByXPath(word4).Text;
                        String w4 = "/" + getWord(Path4);
                        w1 = w1 + w4;

                        Thread.Sleep(1000);
                        FindByXPath(Prefix + "ul/li[" + i + "]/ul/li[" + j + "]/ul/li[" + k + "]/" + Postfix).Click();
                        var x2 = Prefix + "ul/li[" + i + "]/ul/li[" + j + "]/ul/li[" + k + "]/ul";
                        var y2 = FindByXPath(x2);
                        int z2 = nodeNumber(y2);
                        for (int l=1;l<=z2;l++)
                        {
                            String word5 = Prefix + "ul/li[" + i + "]/ul/li[" + j + "]/ul/li[" + k + "]/ul/li[" + l + "]/span/span[3]";
                            String Path5 = FindByXPath(word5).Text;
                            String w5 = "/" + getWord(Path5);
                            w1 = w1 + w5;

                            Thread.Sleep(1000);
                            FindByXPath(Prefix + "ul/li[" + i + "]/ul/li[" + j + "]/ul/li[" + k + "]/ul/li[" + l + "]/" + Postfix).Click();
                            var x3 = Prefix + "ul/li[" + i + "]/ul/li[" + j + "]/ul/li[" + k + "]/ul/li[" + l + "]/ul";
                            var y3 = FindByXPath(x3);
                            int z3 = nodeNumber(y3);
                            for (int m = 1; m <= z3; m++)
                            {
                                String word6 = Prefix + "ul/li[" + i + "]/ul/li[" + j + "]/ul/li[" + k + "]/ul/li[" + l + "]/ul/li[" + m + "]/span/span[3]";
                                String Path6 = FindByXPath(word6).Text;
                                String w6 = "/" + getWord(Path6);
                                w1 = w1 + w6;

                                Thread.Sleep(1000);
                                    FindByXPath(Prefix + "ul/li[" + i + "]/ul/li[" + j + "]/ul/li[" + k + "]/ul/li[" + l + "]/ul/li[" + m + "]/" + Postfix).Click();
                                    var x4 = Prefix + "ul/li[" + i + "]/ul/li[" + j + "]/ul/li[" + k + "]/ul/li[" + l + "]/ul/li[" + m + "]/ul";
                                    var y4 = FindByXPath(x4);
                                    int z4 = nodeNumber(y4);
                                    for (int n = 1; n <= z4; n++)
                                    {
                                    String word7 = Prefix + "ul/li[" + i + "]/ul/li[" + j + "]/ul/li[" + k + "]/ul/li[" + l + "]/ul/li[" + m + "]/ul/li[" + n + "]/span/span[3]";
                                    String Path7 = FindByXPath(word7).Text;
                                    String w7 = "/" + getWord(Path7);
                                    w1 = w1 + w7;
                                    Thread.Sleep(1000);
                                        
                                            FindByXPath(Prefix + "ul/li[" + i + "]/ul/li[" + j + "]/ul/li[" + k + "]/ul/li[" + l + "]/ul/li[" + m + "]/ul/li[" + n + "]/" + Postfix).Click();
                                            Thread.Sleep(1000);

                                            var x5 = Prefix + "ul/li[" + i + "]/ul/li[" + j + "]/ul/li[" + k + "]/ul/li[" + l + "]/ul/li[" + m + "]/ul/li[" + n + "]/ul";
                                            var y5 = FindByXPath(x5);
                                            int z5 = nodeNumber(y5);

                                            int nbrWord = wordNumber(y5);
                                            for (int o= 1; o<= nbrWord; o++)
                                            {
                                                String A = _gar[p].Path;
                                                if (m == 1 )
                                                    { String word8 = Prefix + "ul/li[" + i + "]/ul/li[" + j + "]/ul/li[" + k + "]/ul/li[" + l + "]/ul/li/ul/li[" + n + "]/ul/li[" + o + "]/span/span[3]";
                                                        String Path8 = FindByXPath(word8).Text;
                                                        String w8 = "/" + getWord(Path8);
                                                        String LastPath = w1 + w8;
                                                        String removed = removeLastWord(LastPath, w8);
                                                        comparer(A, LastPath);
                                                        var verif1 = A;
                                                        var verif2 = LastPath;
                                                    }
                                                            else
                                                    { String word8 = Prefix + "ul/li[" + i + "]/ul/li[" + j + "]/ul/li[" + k + "]/ul/li[" + l + "]/ul/li[" + m + "]/ul/li[" + n + "]/ul/li[" + o + "]/span/span[3]";
                                                        String Path8 = FindByXPath(word8).Text;
                                                        String w8 = "/" + getWord(Path8);
                                                        String LastPath = w1 + w8;
                                                        String removed = removeLastWord(LastPath, "/"+w8);
                                                        comparer(A, LastPath);
                                                        var verif1 = A;
                                                        var verif2 = LastPath;
                                                    }
                                                
                                                p++;
                                             
                                            }

                                    FindByXPath(Prefix + "ul/li[" + i + "]/ul/li[" + j + "]/ul/li[" + k + "]/ul/li[" + l + "]/ul/li[" + m + "]/ul/li[" + n + "]/" + Postfix).Click();
                                    w1= removeLastWord(w1, w7);
                                }
                                    FindByXPath(Prefix + "ul/li[" + i + "]/ul/li[" + j + "]/ul/li[" + k + "]/ul/li[" + l + "]/ul/li[" + m + "]/" + Postfix).Click();

                                w1 = removeLastWord(w1, w6);
                            }
                            FindByXPath(Prefix + "ul/li[" + i + "]/ul/li[" + j + "]/ul/li[" + k + "]/ul/li[" + l + "]/" + Postfix).Click();
                            w1 = removeLastWord(w1, w5);
                        }
                        FindByXPath(Prefix + "ul/li[" + i + "]/ul/li[" + j + "]/ul/li[" + k + "]/" + Postfix).Click();
                        w1 = removeLastWord(w1, w4);
                    }
                    FindByXPath(Prefix + "ul/li[" + i + "]/ul/li[" + j + "]/" + Postfix).Click();
                    w1 = removeLastWord(w1, w3);
                }
                FindByXPath(Prefix + "ul/li[" + i + "]/" + Postfix).Click();
                w1 = removeLastWord(w1, w2);
            }

        }
        #region comparer
        public void comparer(String Path1, String Path2)
        {
            if(Path1 == Path2)
            {
                Console.WriteLine("\nLe contenu Schedule position du Gar : " + Path1 + " est conforme au Kronos : "+Path2);
                Console.WriteLine("Passed");
            }
            else
            {
                Console.WriteLine("\nLe contenu Schedule position du Gar : " + Path1 + " ne correspond pas au Kronos : "+Path2);
                Console.WriteLine("Failed");
            }
        }
        #endregion

        #region verifPath
        public Boolean verifPath(String word)
        {
            return true;
        }
        #endregion

        #region removeLastWord
        public string removeLastWord(String Path, String toRemove)
        {
            string Word = string.Empty;
            int i = Path.IndexOf(toRemove);
            if (i >= 0)
            {
                Word = Path.Remove(i, toRemove.Length);
            }
            return Word.TrimEnd();
        }
        #endregion

        #region getWord
        public string getWord(String Path)
        {
            string toRemove = "Beginning Of Time - Forever";
            string Word = string.Empty;
            int i = Path.IndexOf(toRemove);
            if (i >= 0)
            {
                Word = Path.Remove(i, toRemove.Length);
            }
            return Word.TrimEnd();
        }
        #endregion

        #region nodeNumber
        public int nodeNumber(IWebElement Node)
        {
            //var Nodetest = FindByXPath("//*[@id=\"mainWidgetContent\"]/ui-view/ng-form/div/div/div/div/div/div/fieldset/krn-tree-configurator/div/div/div/div/krn-lazy-tree/ul/li/ul/li/ul");
            
            try
            {
                var result = Node.GetAttribute("innerHTML");
                var exp = "dynatree-has-children";
                var liste = result.Split(' ');
                int v = 0;
                foreach (var wrd in liste)
                {

                    if (wrd == exp)
                        v++;

                }
                return v;
            }
            catch { return 0; }
        }
        #endregion

        #region wordNumber
        public int wordNumber(IWebElement Node)
        {
            
            try
            {
                var result = Node.GetAttribute("innerHTML");
                var exp = "dynatree-exp-c";
                var liste = result.Split(' ');
                int v = 1;
                foreach (var wrd in liste)
                {

                    if (wrd == exp)
                        v++;

                }
                return v;
            }
            catch { return 0; }
        }
        #endregion

        #region nodeClick
        public Boolean nodeClick(IWebElement Node)
        {
            try
            {
                var result = Node.GetAttribute("innerHTML");
                var exp = "dynatree-title_job";
                var liste = result.Split(' ');
                int v = 0;
                foreach (var wrd in liste)
                {

                    if (wrd == exp)
                        v++;

                }
                if (v != 0) { return true; }
                else return false;
            }
            catch
            { return false; }
        }

        #endregion

        #region Comparaison
        public Boolean comparerGar(int i, int tmp, int nbrligne)
        {
            if ((_gar[i - 1].LocationName == _gar[i].LocationName) && (_gar[i - 1].FinanceCostCenter == _gar[i].FinanceCostCenter))
            {                                   
                
                String Text = FindByXPath("//*[@id=\"mainWidgetContent\"]/ui-view/ng-form/div/div/div/div/div/div/fieldset/krn-tree-configurator/div/div/div/div/krn-lazy-tree/ul/li/ul/li/ul/li[1]/ul/li[1]/ul/li[2]/ul/li[1]/ul/li/ul/li[4]/ul/li[" + tmp + "]/span/span[3]").Text;
                    String word = Text.Split(' ').First();
                    var t1 = word.Contains(_gar[i - 1].SchedPosition);
                    var t2 = _gar[i - 1].SchedPosition.Contains(word);
                    if (t1 == true || t2 == true) { Console.WriteLine("Le contenu de Schedule position du Gar : " + _gar[i - 1].SchedPosition + " est identique au Kronos"); }
                    else { Console.WriteLine("Le contenu Schedule position du Gar : " + _gar[i-1].SchedPosition + " ne correspond pas au Kronos"); }
                return true;
            }
            else
            {
                

                if (nbrligne > 1)
                {
                    String Text = FindByXPath("//*[@id=\"mainWidgetContent\"]/ui-view/ng-form/div/div/div/div/div/div/fieldset/krn-tree-configurator/div/div/div/div/krn-lazy-tree/ul/li/ul/li/ul/li[1]/ul/li[1]/ul/li[2]/ul/li[1]/ul/li/ul/li[4]/ul/li[" + tmp + "]/span/span[3]").Text;
                    String word = Text.Split(' ').First();
                    var t1 = word.Contains(_gar[i - 1].SchedPosition);
                    var t2 = _gar[2].SchedPosition.Contains(word);
                    if (t1 == true || t2 == true) { Console.WriteLine("Le contenu de Schedule position du Gar : " + _gar[i - 1].SchedPosition + " est identique au Kronos"); }
                    else { Console.WriteLine("Le contenu Schedule position du Gar : " + _gar[i - 1].SchedPosition + " ne correspond pas au Kronos"); }
                    nbrligne = 1;
                }
                else
                {                               //*[@id="mainWidgetContent"]/ui-view/ng-form/div/div/div/div/div/div/fieldset/krn-tree-configurator/div/div/div/div/krn-lazy-tree/ul/li/ul/li/ul/li[1]/ul/li[1]/ul/li[2]/ul/li[1]/ul/li/ul/li[5]/ul/li/span/span[3]
                    String Text = FindByXPath("//*[@id=\"mainWidgetContent\"]/ui-view/ng-form/div/div/div/div/div/div/fieldset/krn-tree-configurator/div/div/div/div/krn-lazy-tree/ul/li/ul/li/ul/li[1]/ul/li[1]/ul/li[2]/ul/li[1]/ul/li/ul/li["+i+1+"]/ul/li/span/span[3]").Text;
                    String word = Text.Split(' ').First();
                    var t1 = word.Contains(_gar[i - 1].SchedPosition);
                    var t2 = _gar[i-1].SchedPosition.Contains(word);
                    if (t1 == true || t2 == true) { Console.WriteLine("Le contenu de Schedule position du Gar : " + _gar[i - 1].SchedPosition + " est identique au Kronos"); }
                    else { Console.WriteLine("Le contenu Schedule position du Gar : " + _gar[i - 1].SchedPosition + " ne correspond pas au Kronos"); }
                }
                
                return false;
            }
        }
        #endregion
        #region fichier Gar
        public List<Gar> ListEmployees()
        {
            List<Gar> _produits = new List<Gar>();
            foreach (ExcelWorksheet workSheet in docGar.Workbook.Worksheets)
            {
                if (workSheet.Name == "Organizational Map")  //workSheet.Name.ToLower()
                {
                    var start = workSheet.Dimension.Start;
                    var end = workSheet.Dimension.End;
                    for (int row = start.Row + 1; row <= end.Row; row++)
                    {
                        ExcelRange range = workSheet.Cells[row, start.Column, row, end.Column];
                        Gar _produitConf = new Gar();
                        if (workSheet.Cells["A" + row].Value == null) { _produitConf.SiteName = ""; }
                        else { _produitConf.SiteName = workSheet.Cells["A" + row].Value.ToString(); }//A;
                        if (workSheet.Cells["B" + row].Value == null) { _produitConf.PropGroupName = ""; }
                        else { _produitConf.PropGroupName = workSheet.Cells["B" + row].Value.ToString(); }//B;
                        if (workSheet.Cells["C" + row].Value == null) { _produitConf.PropName = ""; }
                        else { _produitConf.PropName = workSheet.Cells["C" + row].Value.ToString(); }//C;
                        if (workSheet.Cells["D" + row].Value == null) { _produitConf.LineBusName = ""; }
                        else { _produitConf.LineBusName = workSheet.Cells["D" + row].Value.ToString(); }//D;
                        if (workSheet.Cells["E" + row].Value == null) { _produitConf.BusinessName = ""; }
                        else { _produitConf.BusinessName = workSheet.Cells["E" + row].Value.ToString(); }//E;
                        if (workSheet.Cells["F" + row].Value == null) { _produitConf.LocationName = ""; }
                        else { _produitConf.LocationName = workSheet.Cells["F" + row].Value.ToString(); }//F;
                        if (workSheet.Cells["G" + row].Value == null) { _produitConf.FinanceCostCenter = ""; }
                        else { _produitConf.FinanceCostCenter = workSheet.Cells["G" + row].Value.ToString(); }//G;
                        if (workSheet.Cells["H" + row].Value == null) { _produitConf.SchedPosition = ""; }
                        else { _produitConf.SchedPosition = workSheet.Cells["H" + row].Value.ToString(); }//H;
                        if (workSheet.Cells["I" + row].Value == null) { _produitConf.Skills = ""; }
                        else { _produitConf.Skills = workSheet.Cells["I" + row].Value.ToString(); }//I;
                        if (workSheet.Cells["J" + row].Value == null) { _produitConf.Path = ""; }
                        else { _produitConf.Path = workSheet.Cells["J" + row].Value.ToString(); }//J;
                        _produits.Add(_produitConf);
                    }
                }
            }

            return _produits;
        }
        #endregion



        #region Find By
        /// <summary>
        ///  Retourner l'element et afficher le log
        /// </summary>
        /// <param name="Xpath"></param>
        /// <returns></returns>
        public IWebElement FindByXPath(string Xpath)
        {
            TestContext _cntx = TestContext.CurrentContext;
            element = null;
            if (attemps < 2)
            {
                try
                {
                    //Thread.Sleep(3000);
                    //Console.WriteLine("\n Recuperation de l'element -> " + Xpath);
                    element = _wait.Until(ExpectedConditions.ElementExists(By.XPath(Xpath)));
                    //Console.WriteLine("Ok");
                    attemps = 0;
                }
                catch (Exception ex)
                {
                    attemps++;
                    FindByXPath(Xpath);
                }
            }
            return element;
        }

        //Retourner l'element et afficher le log
        public IReadOnlyCollection<IWebElement> FindByClassName(string ClassName)
        {
            TestContext _cntx = TestContext.CurrentContext;
            IReadOnlyCollection<IWebElement> elements = new List<IWebElement>();
            if (attemps < 2)
            {
                try
                {
                    //Thread.Sleep(1500);
                    Console.WriteLine("Recuperation de l'element -> " + ClassName);
                    elements = _wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(By.ClassName(ClassName)));
                    Console.WriteLine("Ok");
                    attemps = 0;
                }
                catch (Exception ex)
                {
                    attemps++;
                    FindByClassName(ClassName);
                }
            }
            return elements;
        }
        //Retourner l'element et afficher le log
        public IWebElement FindById(string Id)
        {
            TestContext _cntx = TestContext.CurrentContext;
            element = null;
            if (attemps < 2)
            {
                try
                {
                    //Thread.Sleep(3000);
                    Console.WriteLine("Recuperation de l'element -> " + Id);
                    element = _wait.Until(ExpectedConditions.ElementExists(By.Id(Id)));
                    if (element.Text != "")
                    {
                        //Console.WriteLine("L'element a le text suivant : " + element.Text);
                    }
                    Console.WriteLine("Ok");
                    attemps = 0;
                }
                catch (Exception ex)
                {
                    attemps++;
                    FindById(Id);
                }
            }

            return element;
        }
        #endregion
    }
}